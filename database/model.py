from pony.orm import *
import json

with open('config.json', 'r') as con:
    data = json.load(con)
db = data['database']
database = Database()
database.bind(provider='postgres', database=db['name'], user=db['user'], password=db['password'], host=db['host'])


class Type_Document(database.Entity):
    name = Required(str, unique=True)
    alternate_name = Required(str, unique=True)
    claim_registrys = Set("Claim_Registry")


class Claim_Registry(database.Entity):
    id_doc = Required(Type_Document)
    num_doc = Required(str)
    names = Required(str)
    last_nam = Required(str)
    email = Required(str)
    telf = Required(str)
    city = Required(str)
    province = Required(str)
    district = Required(str)
    complaint = Required(str)
    attended = Required(bool, default=False)


sql_debug(False)
database.generate_mapping(create_tables=True)
