#start server
gunicorn -b :8888 app:api

#enter in form
http://localhost:8888/libro

#CREATE DATABASE USER
create database complaints_book;
create user indec with encrypted password 'Indec0p1';
grant all privileges on database complaints_book to indec;

#CREATE TABLE

CREATE TABLE type_document(
	id SERIAL PRIMARY KEY,
	name varchar(64) not null UNIQUE,
	alternate_name varchar(64) not null UNIQUE
);

CREATE TABLE claim_registry(
	id SERIAL PRIMARY KEY,
	id_doc integer REFERENCES "type_document"("id"),
	num_doc varchar(64) not null,
	names varchar(128) not null,
	last_nam varchar(128) not null,
	email varchar(64) not null,
	telf varchar(64) not null,
	city varchar(64) not null,
	province varchar(64) not null,
	district varchar(64) not null,
	complaint text not null,
	attended boolean not null
);

