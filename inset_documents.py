from database.model import *

with db_session:
    if Type_Document.get(name="D.N.I") is None:
        p1 = Type_Document(name="D.N.I", alternate_name="DNI")
        commit()

    if Type_Document.get(name="R.U.C") is None:
        p2 = Type_Document(name="R.U.C", alternate_name="RUC")
        commit()

    if Type_Document.get(name="CARNET DE EXTRANJERIA") is None:
        p3 = Type_Document(name="CARNET DE EXTRANJERIA", alternate_name="CARNET DE EXTRANJERIA")
        commit()

    if Type_Document.get(name="PASAPORTE") is None:
        p4 = Type_Document(name="PASAPORTE", alternate_name="PASAPORTE")
        commit()

    if Type_Document.get(name="BREVETE") is None:
        p5 = Type_Document(name="BREVETE", alternate_name="BREVETE")
        commit()