import falcon
import os
from database.model import *
import json
from falcon.http_status import HTTPStatus
import jinja2


class HandleCORS(object):
    def process_request(self, req, resp):
        resp.set_header('Access-Control-Allow-Origin', '*')
        resp.set_header("content-type", "application/json")
        resp.set_header('Access-Control-Allow-Methods', '*')
        resp.set_header('Access-Control-Allow-Headers', '*')
        resp.set_header('Access-Control-Max-Age', 1728000)  # 20 days
        if req.method == 'OPTIONS':
            raise HTTPStatus(falcon.HTTP_200, body='\n')


def load_template(name):
    path = os.path.join('templates', name)
    with open(os.path.abspath(path), 'r') as fp:
        return jinja2.Template(fp.read())


class libro:
    def on_get(self, req, resp):
        template = load_template('libro.html')
        resp.status = falcon.HTTP_200
        resp.content_type = 'text/html'
        resp.body = template.render(something='testing')


class schema:
    def on_get(self, req, resp):
        with open('json/schema.json') as fr:
            schema1 = json.load(fr)
            resp.body = json.dumps(schema1)


class options:
    def on_get(self, req, resp):
        with open('json/options.json') as fr:
            option1 = json.load(fr)
            resp.body = json.dumps(option1)


class register:
    def on_post(self, req, resp):
        data = req.stream.read().decode('utf-8')
        l_data = data.split('&')
        dic_data = {}
        for x in l_data:
            dic_data[x.split('=')[0]] = x.split('=')[1].replace('+',' ')
        with db_session:
            try:
                con_id = Type_Document.get(name=dic_data['type_doc'])
                id_doc2 = int(con_id.id)
            except Exception as e:
                print(e)
            try:
                if Claim_Registry.get(id_doc=con_id, num_doc=dic_data['num_doc'], names=dic_data['nombres'],
                                      last_nam=dic_data['apellidos'], email=dic_data['correo'],
                                      telf=dic_data['telefono'], city=dic_data['ciudad'],
                                      province=dic_data['provincia'], district=dic_data['distrito'],
                                      complaint=dic_data['reclamo']) is None:

                    p1 = Claim_Registry(id_doc=con_id, num_doc=dic_data['num_doc'], names=dic_data['nombres'],
                                        last_nam=dic_data['apellidos'], email=dic_data['correo'],
                                        telf=dic_data['telefono'], city=dic_data['ciudad'],
                                        province=dic_data['provincia'], district=dic_data['distrito'],
                                        complaint=dic_data['reclamo'])
                    commit()
            except Exception as e:
                print(e)


class javaScript(object):
    def on_get(self, req, resp, filename):
        resp.status = falcon.HTTP_200
        resp.content_type = 'appropriate/content-type'
        with open("templates/js/" + filename, 'r') as f:
            resp.body = f.read()


class css(object):
    def on_get(self, req, resp, filename):
        resp.status = falcon.HTTP_200
        resp.content_type = 'text/css'
        with open("templates/css/" + filename, 'r') as f:
            resp.body = f.read()


api = falcon.API(middleware=[HandleCORS()])
api.add_route('/libro', libro())
api.add_route('/schema', schema())
api.add_route('/options', options())
api.add_route('/register', register())
api.add_route('/js/{filename}', javaScript())
api.add_route('/css/{filename}', css())
